import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        System.out.println("----------------------------------------------------------");
        System.out.println("Введите число 1, 2 или 3");

        Scanner inputFigure = new Scanner(System.in);
        if (inputFigure.hasNextInt()){
            // если введенное значение целое число, то:
            int num = inputFigure.nextInt();
            System.out.println("Используем if/else");

            if (num < 1 || num > 3){
                System.out.println("Неверное число!");
            }else if (num == 1){
                System.out.println("Вы ввели: 1");
            }else if(num == 2){
                System.out.println("Вы ввели: 2");
            }else {
                System.out.println("Вы ввели: 3");
            }

            System.out.println("");
            System.out.println("Используем Switch");

            switch (num){
                case (1):
                    System.out.println("Вы ввели: 1");
                    break;
                case (2):
                    System.out.println("Вы ввели: 2");
                    break;
                case (3):
                    System.out.println("Вы ввели: 3");
                    break;
                default:
                    System.out.println("Неверное число!");
            }
        }else {
            //если число не целое или символ
            System.out.println("Вы ввели не целое число!!!");
        }

        System.out.println("");
        System.out.println("----------------------------------------------------------");
        System.out.println("вывести на экран числа от 5 до 1");

        for (int i = 5; i > 0; i--){
            System.out.print(i+" ");
        }

        System.out.println("");
        System.out.println("----------------------------------------------------------");
        System.out.println("вывести на экран таблицу умножения на 3");
        for (int i = 1; i <= 10; i++){
            System.out.println("3x"+i+"="+ 3*i);
        }

        System.out.println("");
        System.out.println("----------------------------------------------------------");
        System.out.println("Напишите программу котороя будет заполнять масив рандомными числами");
        System.out.println("Введите длинну массива");

        Scanner inputInt = new Scanner(System.in);
        if (inputInt.hasNextInt()) {
            int arrayLength = inputInt.nextInt();
            int [] array = new int[arrayLength];
            for (int i = 0; i < arrayLength; i++){
                array[i] = new Random().nextInt(arrayLength);
            }
            System.out.print("номер элемента: "+ "\t");
            for (int i = 0; i < arrayLength; i++){
                System.out.print(i+1+"\t");
            }
            System.out.println("");
            System.out.print("значение элемента: "+"\t");
            for (int i = 0; i < arrayLength; i++){
                System.out.print(array[i]+"\t");
            }
            }else {
            System.out.println("Введено не целое число!");
        }

    }
}
